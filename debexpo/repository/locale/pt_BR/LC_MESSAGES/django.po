# Portuguese translations for mentors.debian.net website (debexpo).
# This file is distributed under the same license as debexpo.
# https://salsa.debian.org/mentors.debian.net-team/debexpo
# Thiago Pezzo (tico) <pezzo@protonmail.com>, 2022.
# Carlos Henrique Lima Melara <charlesmelara@outlook.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: debexpo\n"
"Report-Msgid-Bugs-To: <debian-l10n-portuguese@lists.debian.org>\n"
"POT-Creation-Date: 2022-09-26 20:33+0000\n"
"PO-Revision-Date: 2022-09-11 17:54-0300\n"
"Last-Translator: Thiago Pezzo (tico) <pezzo@protonmail.com>\n"
"Language-Team: debian-l10n-portuguese\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 3.1.1\n"

#: repository/models.py:72
msgid "Name"
msgstr "Nome"

#: repository/models.py:73
msgid "Version"
msgstr "Versão"

#: repository/models.py:74
msgid "Component"
msgstr "Componente"

#: repository/models.py:76
msgid "Distribution"
msgstr "Distribução"

#: repository/models.py:78
msgid "Path"
msgstr "Caminho"

#: repository/models.py:79
msgid "Size"
msgstr "Tamanho"

#: repository/models.py:80
msgid "SHA256"
msgstr "SHA256"
